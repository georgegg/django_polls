from django.db import models
import datetime
from django.utils import timezone

class Question(models.Model):
    question_text = models.CharField(max_length=200)
    published_at = models.DateTimeField('published at')

    def was_published_recently(self):
        return self.published_at >= timezone.now() - datetime.timedelta(days=1)
    was_published_recently.admin_order_field = 'published_at'
    was_published_recently.boolean = True
    was_published_recently.short_description = 'Published recently?'

class Choice(models.Model):
    question = models.ForeignKey(Question)
    choice_text = models.CharField(max_length=200)
    votes = models.IntegerField(default=0)