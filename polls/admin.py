from django.contrib import admin
from polls.models import Choice, Question

class ChoiceInline(admin.TabularInline):
    model = Choice
    extra = 1

class QuestionAdmin(admin.ModelAdmin):
    fieldsets = [
        ('Question', {'fields': ['question_text']}),
        ('Date information', {'fields': ['published_at']}),
    ]
    inlines = [ChoiceInline]
    list_display = ('question_text', 'published_at', 'was_published_recently')
    list_filter = ['published_at']
    search_fields = ['question_text']

admin.site.register(Question, QuestionAdmin)