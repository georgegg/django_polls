Vagrant
========

```
$ cd /to/project/folder
$ vagrant init
```

Boxes are added to Vagrant with vagrant box add. This stores the box under a specific name so that multiple Vagrant environments can re-use it. 
Add one:

```
$ vagrant box add hashicorp/precise32
$ vagrant up
$ vagrant ssh
```


Vagrantfile check!!!!